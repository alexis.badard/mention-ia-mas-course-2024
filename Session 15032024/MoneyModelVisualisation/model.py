import mesa
from mesa.datacollection import DataCollector

from mesa import Agent, Model
from mesa.time import RandomActivation
from mesa.space import MultiGrid
from agents import MoneyAgent


def compute_gini(model):
    agent_wealths = [agent.wealth for agent in model.schedule.agents]
    x = sorted(agent_wealths)
    N = model.num_agents
    B = sum( xi * (N-i) for i,xi in enumerate(x) ) / (N*sum(x))
    return (1 + (1/N) - 2*B)


class MoneyModel(Model):
    """A model with some number of agents."""
   # density is used only for illustartion
    def __init__(self, N, width, height,density=0.8):
        super().__init__()
        self.num_agents = N
        self.grid = MultiGrid(width, height, True)
        self.schedule = RandomActivation(self)
        self.denstiy= width/height #used only for illustration for the slider density
        

        # Create agents
        for i in range(self.num_agents):
            a = MoneyAgent(i, self)
            self.schedule.add(a)
            # Add the agent to a random grid cell
            x = self.random.randrange(self.grid.width)
            y = self.random.randrange(self.grid.height)
            self.grid.place_agent(a, (x, y))

        self.datacollector = DataCollector(
            model_reporters={"Gini": compute_gini},
            agent_reporters={"Wealth": "wealth"})
        
        self.running = True

    def step(self):
        self.schedule.step()
        self.datacollector.collect(self)
        
    def run_model(self, step_count=100):
        model = MoneyModel(50, 10, 10)  # 50 agents in our example
        for i in range(step_count):
            self.step()


        

